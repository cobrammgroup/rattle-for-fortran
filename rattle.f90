PROGRAM rattle
    IMPLICIT NONE

    ! ============================================================================================================================
    !              THESE ARE DUMMY VARIABLES THAT MATCH VARIABLES ALREADY DEFINED IN SHARC
    !                 AND ARE DEFINED HERE JUST FOR CONSISTENCY IN VARIABLE NAMING
    ! ============================================================================================================================

    integer, parameter :: N = 2

    type ctrl_type
        integer :: natom = N                      !< number of atoms
        real*8 :: dtstep = 0.1                    !< length of timestep in a.u (atu)
    endtype ctrl_type

    type trajectory_type
        real*8,dimension(N) :: mass_a = 0.0                       !< atomic mass in a.u. (1 a.u. = rest mass of electron m_e)
        real*8,dimension(N,3) :: geom_ad = 0.0                    !< Cartesian coordinates of atom in a.u. (bohr)
        real*8,dimension(N,3) :: veloc_ad = 0.0                   !< Cartesian velocity in a.u. (bohr/atu)
        real*8,dimension(N,3) :: accel_ad = 0.0                   !< Cartesian acceleration in a.u. (bohr/atu/atu)
        real*8,dimension(N,3) :: grad_ad = 0.0                    !< final gradient used in velocity verlet
    endtype trajectory_type

    integer :: iatom, idir
    type(ctrl_type) :: ctrl
    type(trajectory_type) :: traj

    ! ============================================================================================================================
    !                  THESE ARE NEW VARIABLES REQUIRED BY RATTLE
    ! ============================================================================================================================

    integer, parameter :: nconstraints = 1                  ! nr of constraints, this should be set from input
    integer, dimension(nconstraints,2) :: atom_const        ! atom couples that are constrained to have fixed distance
    real*8, dimension(nconstraints,3) :: initdistvec        ! initial distance vectors between constrained atoms, updated at the beginning of each step
    real*8, dimension(nconstraints) :: D2ini                ! initial distance squared norm between constrained atoms, stored once at the beginning

    real*8, parameter :: rattle_tolerance = 1.E-7           ! tolerance to break the rattle iterations

    logical, dimension(nconstraints) :: check_constraints = .FALSE.    ! logical checks for the RATTLE loops

    integer :: iconst                                       ! RATTLE loop integer variable
    integer :: iA, iB                                       ! atom labels, to make the code more readable
    real*8, dimension(3) :: relvel, relpos                  ! relative position and distance between two atoms
    real*8 :: D2t, dABvdAB, constr                           ! quantities computed in RATTLE

    ! ============================================================================================================================

    integer :: istep  ! integer counter for the velocity-verlet loop

    ! dummy definitions of initial conditions, to check if the code works
    traj%mass_a = (/ 1.0, 1.0 /)              ! both particles are 1 au of mass
    traj%geom_ad(1,:) = (/ 0.0, 0.0, 0.0 /)   ! first particle in the origin
    traj%geom_ad(2,:) = (/ 1.0, 0.0, 0.0 /)   ! the other at 1,1,1
    traj%veloc_ad(1,:) = (/ 0.0, -0.05, 0.0 /) ! first particle is moving along y
    traj%veloc_ad(2,:) = (/ 0.0, 0.0, -0.05 /) ! second particle is moving along z
    traj%grad_ad(1,:) = (/ 0.15, 0.1, 0.2 /) ! first particle is moving along y
    traj%grad_ad(2,:) = (/ -0.1, -0.15, -0.1 /) ! second particle is moving along z

    ! ============================================================================================================================
    !    0a)  STORE INITIAL VALUES (this should be done only once at the beginning)
    ! ============================================================================================================================

    ! here we give a trivial initialization of atomdist_const, just to make the code work
    ! but this variable should someway set from input

    atom_const(1,:) = (/ 1, 2 /)

    ! here we compute the squared norm of the distance vectors between each pair of atoms with constrained distance
    ! this distance will be kept for the whole trajectory and never updated
    do iconst = 1, nconstraints
        relpos = traj%geom_ad(atom_const(iconst,1),:) - traj%geom_ad(atom_const(iconst,2),:)
        D2ini(iconst) = DOT_PRODUCT(relpos, relpos)
    end do

    ! ============================================================================================================================

    print*, "INITIAL COORDINATES"
    print*, traj%geom_ad
    print*, "INITIAL VELOCITIES"
    print*, traj%veloc_ad
    print*, "CONSTRAINED DISTANCES"
    do iconst = 1, nconstraints
        relpos = traj%geom_ad(atom_const(iconst,1),:) - traj%geom_ad(atom_const(iconst,2),:)
        PRINT*, "ATOMS ", atom_const(iconst,1), " - ", atom_const(iconst,2), " = ", SQRT(DOT_PRODUCT(relpos, relpos))
    end do

    ! VELOCITY VERLET LOOP

    DO istep = 1, 100

        ! ============================================================================================================================
        !    0b)  STORE INITIAL VALUES (this should be done at the beginning of each Velocity-Verlet step)
        ! ============================================================================================================================

        ! here we store the distance vectors between each pair of atoms with constrained distance
        do iconst = 1, nconstraints
            initdistvec(iconst,:) = traj%geom_ad(atom_const(iconst,1),:) - traj%geom_ad(atom_const(iconst,2),:)
        end do

        ! ============================================================================================================================
        !    1)  VELOCITY-VERLET LOOP TO UPDATE THE COORDINATES, see subroutine VelocityVerlet_xstep, line 58, nuclear.f90
        ! ============================================================================================================================

        do iatom = 1, ctrl%natom
            do idir = 1, 3
                traj%accel_ad(iatom,idir) = -traj%grad_ad(iatom,idir) / traj%mass_a(iatom)
                traj%geom_ad(iatom,idir) = traj%geom_ad(iatom,idir) + traj%veloc_ad(iatom,idir) * ctrl%dtstep &
                                                                & + 0.5d0 * traj%accel_ad(iatom,idir) * ctrl%dtstep**2
            enddo
        enddo

        ! ============================================================================================================================
        !    2)  RATTLE TO SET COORDINATE CONSTRAINTS (add to VelocityVerlet_xstep?)
        ! ============================================================-================================================================

        do

            ! initialize logical variable that controls whether constraints are enforced
            check_constraints(:) = .TRUE.

            ! loop over the constrained bonds
            do iconst = 1, nconstraints

                ! define the atomic id of the atoms that have fixed distance
                iA = atom_const(iconst,1); iB = atom_const(iconst,2)
                ! compute the relative position of the two constrained atoms, and the relative norm squared
                relpos = traj%geom_ad(iA,:) - traj%geom_ad(iB,:)
                D2t = DOT_PRODUCT(relpos, relpos)

                ! when the difference with the fixed distance is significantly different from zero, do RATTLE
                if ( abs(D2t - D2ini(iconst)) > rattle_tolerance ) then
                    ! compute RATTLE coefficient
                    constr = (D2t - D2ini(iconst)) / (2.0 * ctrl%dtstep * DOT_PRODUCT(initdistvec(iconst,:), relpos) * &
                            & (1.0 / traj%mass_a(iA) + 1.0 / traj%mass_a(iB)))
                    ! correct positions
                    traj%geom_ad(iA,:) = traj%geom_ad(iA,:) - (constr * ctrl%dtstep * initdistvec(iconst,:) / traj%mass_a(iA) )
                    traj%geom_ad(iB,:) = traj%geom_ad(iB,:) + (constr * ctrl%dtstep * initdistvec(iconst,:) / traj%mass_a(iB) )
                    ! correct velocities
                    traj%veloc_ad(iA,:) = traj%veloc_ad(iA,:) - (constr * initdistvec(iconst,:) / traj%mass_a(iA) )
                    traj%veloc_ad(iB,:) = traj%veloc_ad(iB,:) + (constr * initdistvec(iconst,:) / traj%mass_a(iB) )
                    ! this constraint was not ok
                    check_constraints(iconst) = .FALSE.
                endif

            end do ! end of the loop over the constraints

            ! break the loop when all constraints are satisfied
            if ( all(check_constraints) ) exit

        end do

        ! ============================================================================================================================
        !    3)  VELOCITY-VERLET LOOP TO UPDATE THE VELOCITIES, see subroutine VelocityVerlet_vstep, line 98, nuclear.f90
        ! ============================================================================================================================

        do iatom = 1, ctrl%natom
            do idir = 1, 3
                traj%accel_ad(iatom,idir) = 0.5d0 * ( traj%accel_ad(iatom,idir) - traj%grad_ad(iatom,idir) / traj%mass_a(iatom) )
                traj%veloc_ad(iatom,idir) = traj%veloc_ad(iatom,idir) + traj%accel_ad(iatom,idir) * ctrl%dtstep
            enddo
        enddo

        ! ============================================================================================================================
        !    4)  RATTLE TO SET VELOCITY CONSTRAINTS (add to VelocityVerlet_vstep?)
        ! ============================================================================================================================

        do

            ! initialize logical variable that controls whether constraints are enforced
            check_constraints(:) = .TRUE.

            ! loop over the constrained bonds
            do iconst = 1, nconstraints

                ! define the atomic id of the atoms that have fixed distance
                iA = atom_const(iconst,1); iB = atom_const(iconst,2)
                ! compute projection of the relative velocity with respect to the distance vector of the bond
                relvel = traj%veloc_ad(iA,:) - traj%veloc_ad(iB,:)
                relpos = traj%geom_ad(iA,:) - traj%geom_ad(iB,:)
                dABvdAB = DOT_PRODUCT(relpos, relvel)

                ! when this projection is significantly different from zero, do RATTLE
                if ( abs(dABvdAB) > rattle_tolerance ) then
                    ! compute RATTLE coefficient
                    constr = dABvdAB / ((1.0 / traj%mass_a(iA) + 1.0 / traj%mass_a(iB)) * D2ini(iconst))
                    ! correct velocities
                    traj%veloc_ad(iA,:) = traj%veloc_ad(iA,:) - (constr * relpos(:) / traj%mass_a(iA) )
                    traj%veloc_ad(iB,:) = traj%veloc_ad(iB,:) + (constr * relpos(:) / traj%mass_a(iB) )
                    ! this constraint was not ok
                    check_constraints(iconst) = .FALSE.
                endif

            end do ! end of the loop over the constraints

            ! break the loop when all constraints are satisfied
            if ( all(check_constraints) ) exit

        end do

    ! ============================================================================================================================

    end do ! end of velocity-verlet loop

    print*, "FINAL COORDINATES"
    print*, traj%geom_ad
    print*, "FINAL VELOCITIES"
    print*, traj%veloc_ad
    print*, "CONSTRAINED DISTANCES"
    do iconst = 1, nconstraints
        relpos = traj%geom_ad(atom_const(iconst,1),:) - traj%geom_ad(atom_const(iconst,2),:)
        PRINT*, "ATOMS ", atom_const(iconst,1), " - ", atom_const(iconst,2), " = ", SQRT(DOT_PRODUCT(relpos, relpos))
    end do

end program
